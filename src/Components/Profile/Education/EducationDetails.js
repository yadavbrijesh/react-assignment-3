import React, { useState } from 'react'
import EducationForm from './EducationForm';
import EducationList from './EducationList';
import { addEdu } from '../../../reduxComponents/actions'
import { connect } from 'react-redux'


const EducationDetails = (props) => {

  const add = props.addEdu;
  const [show, setShow] = useState(false)

  const [course, setCourse] = useState('');
  const [collage, setCollage] = useState('');
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');

  const onFormSubmit = async (e) => {
    e.preventDefault();
    if (!course || !collage || !from || !to) {
      alert("Please enter all fields.")
    }
    else {
      const data = { course, collage, from, to };
      await add(data)
    }
  }

  return (
    <div className="work-exp">
      <p>Education Details</p>
      <button onClick={() => setShow(!show)}>{!show ? "Add" : "Close"}</button>
      {show ? <div>
        <EducationForm
          submit={onFormSubmit}
          Course={e => setCourse(e.target.value)}
          Collage={e => setCollage(e.target.value)}
          From={e => setFrom(e.target.value)}
          To={e => setTo(e.target.value)}
        />
        <EducationList />
      </div>
        : <EducationList />}

    </div>
  )
}


export default connect(null, { addEdu })(EducationDetails)
