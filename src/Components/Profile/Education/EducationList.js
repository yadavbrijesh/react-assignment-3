import React  from 'react'
import { connect } from 'react-redux';
import { delEdu } from '../../../reduxComponents/actions'


const WorkList = (props) => {

  const list = props.list;
  const del = props.delEdu

  const onRemoveClick = async (item) => {

    let edulist = list;
    const filter = edulist.filter((el) => {
      return el.course !== item
    })
    del(filter)
  }


  return (
    <div >
      {list ? list.map(item => {
        return (
          <div className="work-list" key={item.course}>
            <h4>{item.course}</h4>
            <h6>{item.collage}</h6>
            <p><b> From:</b>{` ${item.from}`} <b> To:</b>{` ${item.to}`}</p>
            <button className="ui red button" onClick={() => onRemoveClick(item.course)}>Remove</button>
          </div>
        )
      }) : null}

    </div>
  )
}

const mapStateToProps = (state) => {
  return { list: state.job.Education }
}

export default connect(mapStateToProps, { delEdu })(WorkList)
