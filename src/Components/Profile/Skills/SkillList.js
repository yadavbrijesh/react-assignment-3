import React from 'react'
import {connect} from 'react-redux';
import { delSkill } from '../../../reduxComponents/actions' 

const SkillList = (props) => {

  const list = props.list;
  const del = props.delSkill;

  const onRemoveClick = async (skl) => {
    
    let skill = list;
    const filterSkill = skill.filter((el)=>{
      return el.skill !== skl
    })
    del(filterSkill)
    
}

  return (
    <div >
      {list.length !== 0 ? list.map(item => {
        return (
        <div className="work-list" key={item.skill}>
            <h4>{item.skill}</h4>
            <h5>{item.rating}</h5>
            <button className="ui red button" onClick={() => onRemoveClick(item.skill)}>Remove</button>
        </div>
        )}) : null}
    </div>
  )
}

const mapStateToProps = ( state ) =>{
  return { list : state.job.Skills}
}

export default connect( mapStateToProps, { delSkill } )(SkillList)
