import React, { useState } from 'react'
import SkillForm from './SkillForm';
import SkillList from './SkillList';
import { addSkill } from '../../../reduxComponents/actions'
import { connect } from 'react-redux'

const Skills = (props) => {
  
  const add = props.addSkill;

  const [show, setShow] = useState(false)

  const [skill, setSkill] = useState('');
  const [rating, setRating] = useState('');


  const onFormSubmit = async (e) => {
    e.preventDefault();
    if (!skill || !rating) {
      alert("Please enter all fields.")
    }
    else {
      const data = { skill, rating };
      add(data)
    }
  }

  return (
    <div className="work-exp">
      <p>Skills and Technology</p>
      <button onClick={() => setShow(!show)}>{!show ? "Add" : "Close"}</button>
      {show ? <div>
        <SkillForm
          submit={onFormSubmit}
          skill={e => setSkill(e.target.value)}
          rating={e => setRating(e.target.value)}
        />
        <SkillList />
      </div>
        : <SkillList />
      }

    </div>
  )
}


export default connect( null , { addSkill })(Skills)
