import React, { Component } from 'react'
import PersonalInfo from './PersonalInfo'
import WorkExp from './WorkExp/WorkExp'
import EducationDetails from './Education/EducationDetails'
import Skills from './Skills/Skills'

export class Profile extends Component {
  render() {
    return (
      <div style={{display:"flex"}}>
        {/* <h1 style={{textAlign:"center"}}>This is user profile.</h1> */}
        <PersonalInfo/>
        <div style={{margin:'10px'}}>
        <WorkExp/>
        <EducationDetails/>
        <Skills/>
        </div>
      </div>
    )
  }
}

export default Profile
