import React from 'react'
import './JobDetails.css'

const JobDetails = ({selectedJob}) => {
  const {name,location,description,salary,logo} = selectedJob;
  const {city,country} = location;

  return (
    <div className="job-detail">
        <h2>{name}</h2>
        <h4>{`${city},${country}`}</h4>
        <img src={logo} alt=""/>
        <p className="des">{description}</p>
        <div className="sal"><b>Salary: </b>{salary/1000}K /-</div>
        <button className="ui button"> Apply</button>
        <button className="ui red button">Not interested</button>

    </div>
  )
}

export default JobDetails
