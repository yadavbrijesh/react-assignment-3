import React from 'react'
import { Link } from 'react-router-dom'
import './Nav.css'
import GoogleAuth from '../GoogleAuth/GoogleAuth'
import { connect } from 'react-redux'

const Nav = (props) => {

  return (
    <div className="navbar">
        <Link className="link" to="/">Home</Link>
        <Link className="link" to="/profile">My Profile</Link>
        <Link className="link" to="/companies">See Companies</Link>
        <div className="user">
        <p><b>{props.name ? props.name : "User"}</b></p>
        <img src={props.image ? props.image : "https://www.seekpng.com/png/detail/115-1150053_avatar-png-transparent-png-royalty-free-default-user.png"} alt=""/>
        
        </div>
        <GoogleAuth />
        
         {/* <GoogleLogin 
        icon={false}
        className="login"
        clientId="384505260599-a7ab9t3ktvs1h5scifrvegggl9k794s2.apps.googleusercontent.com"
        buttonText="Login"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
        isSignedIn={true}
        /> */}
        
    </div>
  )
}

const mapStateToProps = (state) =>{
  return {
    name : state.auth.name,
    image : state.auth.image
  }
}
export default connect( mapStateToProps )(Nav)
